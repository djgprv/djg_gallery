djg_gallery
----
* Advanced gallery for WolfCMS

FUNCTIONALITY
----

* page as gallery
* multiupload
* drag drop sortable files
* tumbnails (resize, crop, watermarks) 
* display albums, gallery, carousel
* download gallery as zip file
* edit title and description of file on fly (jquery.jeditable.js by Mika Tuupola, Dylan Verheul)
* colorbox fronted html5 compatible

HOW TO US
----

* Copy files to the wolf/plugins/djg_gallery

**** Insert HEAD section: **** 
```sh
<!-- djg_gallery -->
<link rel="stylesheet" type="text/css" href="<?php echo URL_PUBLIC; ?>wolf/plugins/djg_gallery/css/djg_gallery_fronted.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URL_PUBLIC; ?>wolf/plugins/djg_gallery/assets/colorbox/themes/simple_dark/colorbox.css" />
<script type="text/javascript" src="<?php echo URL_PUBLIC; ?>wolf/plugins/djg_gallery/assets/jquery.min.js"></script>
<script type="text/javascript"src=" <?php echo URL_PUBLIC; ?>wolf/plugins/djg_gallery/assets/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript">
<![CDATA[
function ON_READY() {
jQuery(".link_colorbox").each(function() {
var obj = jQuery(this);    var obj_settings = {};
if(obj.data("lightbox")){var jObj = jQuery.parseJSON(obj.data("lightbox").toString().replace(/'/g, '"')); obj_settings = jObj; obj_settings.rel = jObj.group; }
obj.colorbox(obj_settings); obj.colorbox({height:"80%", scalePhotos:true});
});
jQuery(".link_colorbox").colorbox();
}
jQuery(document).ready(ON_READY);
]]>
</script>
<!-- djg_gallery end -->
```
**** Insert in layout after echo Page::content(); ****
```sh
<?php
/** djg_gallery_fronted */
if (Plugin::isEnabled('djg_gallery')):
if ($this->djg_gallery == Djggallery::ALBUM) DjgGalleryRender::album($this);
if ($this->djg_gallery == Djggallery::GALLERY) DjgGalleryRender::gallery($this);
if ($this->djg_gallery == Djggallery::CAROUSEL) DjgGalleryRender::carousel($this);
if ( ($this->djg_gallery == Djggallery::GALLERY) && ($this->djg_gallery_download_allow == 1) ) DjgGalleryRender::download($this);
endif;
?>
```

ICON USED
----

* Eightyshades by Victor Erixon: http://www.iconfinder.com/search/?q=iconset%3Aeightyshades
* Bremen by pc.de: http://www.iconfinder.com/icondetails/59399/32/project_icon (modified by kreacjawww.pl)

REQUIRES
----

PHP: BC Math, GD lib, 5.3.x

TO DO
----
clean up the code
regenerate thumnails