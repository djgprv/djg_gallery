<?php

/**
* DjgGalleryRender Class
**/

class Djggalleryrender {
    const AlbumThumbName = "145_";
    const GalleryThumbName = "145_";
	const GalleryLinkClassName = "link_colorbox";
	const ItemsPerPage = 15;
	const DisplayEmptyGallery = true;
	
	private $path = "public/djg_gallery/";
	function Djggalleryrender(){
		$this->path = Plugin::getSetting('path', 'djg_gallery');
	}
	/**
	* donwnload gallery as zip file
	* $page: object or int(page id)
	*/
	public static function download($page)
	{
		$pageId = (is_object($page)) ? $page->id() : $page;
		$pageSlug =  (is_object($page)) ? $page->slug() : Page::findById($page)->slug();
		echo '<a href="' . URL_PUBLIC . 'djg_gallery/download/' . $pageId . '/' . $pageSlug . '">'.__('Download gallery').'</a>';
	}
	/**
	* carousel
	*/
	public static function carousel($page)
	{
		$pageId = (is_object($page)) ? $page->id() : $page;
		self::dispalyThemeGallery(self::getPicsArray($pageId,"djg_gallery"),"djg_flexisel_".$pageId,null);
	}
	/**
	* gallery
	*/
	public static function gallery($page,$pager=false,$limit=false)
	{
		$pageId = (is_object($page)) ? $page->id() : $page;
		if(($pager==true)&&($limit == false)):
			use_helper('Pager');
			$pager = new Pager(array('style' => 'punbb','items_per_page' => self::ItemsPerPage,'total_items' => count(self::getPicsArray($pageId))));
			self::dispalyThemeGallery(self::getPicsArray(array('pageId'=>$pageId,'limit' => $pager->items_per_page, 'offset' => $pager->sql_offset)),"djg_gallery_g");
			echo ($pager->total_items > self::ItemsPerPage) ? $pager : null;
		else:
			if($limit==false):
				self::dispalyThemeGallery(self::getPicsArray($pageId,"djg_gallery"),"djg_gallery_g");
			else:
				self::dispalyThemeGallery(self::getPicsArray(array('pageId'=>$pageId,'limit' => $limit, 'offset' => 0)),"djg_gallery_g",null,true);
			endif;
		endif;
		
	}
	/**
	* album
	*/
	public static function album($page,$pager=false)
	{
		$pageId = (is_object($page)) ? $page->id() : $page;
		if($pager==true):
			use_helper('Pager');
			$pager = new Pager(array('style' => 'punbb','items_per_page' => self::ItemsPerPage,'total_items' => count(self::getAlbumsArray($pageId))));
			self::dispalyThemeAlbum(self::getAlbumsArray(array('pageId'=>$pageId,'limit' => $pager->items_per_page, 'offset' => $pager->sql_offset)),"djg_gallery_a");
			echo ($pager->total_items > self::ItemsPerPage) ? $pager : null;
		else:
			self::dispalyThemeAlbum(self::getAlbumsArray($pageId,"djg_gallery"),"djg_gallery_a");
		endif;
		
	}
	/* 
	* method: getPicsArray
	* parametr: items:Array(itemId, limit, offset)
	* itemId (string or array)
	*/
	public static function getPicByPageId($pageId){
		$sql = "SELECT * FROM ".TABLE_PREFIX."djg_gallery_pics WHERE pageId = $pageId ORDER BY sort_order ASC, id DESC LIMIT 1";
		return Djggallery::executeSql($sql);	
	}
	/* 
	* method: getPicsArray
	* parametr: items:Array(itemId, limit, offset)
	* itemId (string or array)
	*/
	public static function getPicsArray($i)
	{
		if(is_array($i)):
			if(!isset($i['pageId'])) exit('no page id');
			$pageId = (is_array($i['pageId'])) ? implode(',',$i['pageId']) : $i['pageId'];
			if( (isset($i['limit'])) && (isset($i['offset'])) ):
				$offset = $i['offset'];
				$limit = $i['limit'];
				$iLimit=" LIMIT $offset,$limit "; 
			else: 
				$iLimit=""; 
			endif;
		else:
			$pageId = $i;
			$iLimit="";
		endif;
		$sql = "SELECT * FROM ".TABLE_PREFIX."djg_gallery_pics WHERE pageId IN ($pageId)";
		$sql .= " ORDER BY sort_order ASC, id DESC $iLimit ";
		return Djggallery::executeSql($sql);
	} //end getPicsArray	
	
	public static function dispalyThemeGallery($files=array(),$className=null,$idName=null,$moreLink=false)
	{
		$group = time();
		$path = "public/djg_gallery/";
		$id = ($idName==null)?'':'id="'.$idName.'"';
		$class = ($className==null)?'':'class="'.$className.'"';
		echo '<ul '.$id.' '.$class.'>';
		foreach($files as $key=>$val):	
				echo "<li>";
				echo '<a class="'.self::GalleryLinkClassName.'" data-lightbox="{\'group\':\''.$group.'\'}" href="' . URL_PUBLIC . $path . $val['filename'] . '.' . $val['fileext'] . '" title="' . $val['title'] . '">';
				echo '<img src="' . URL_PUBLIC . $path . 'thumbs/' . self::GalleryThumbName . $val['filename'] . '.' . $val['fileext'] . '" alt="'.$val['title'].'" width="145" height="145" /></a>';
				echo "</li>";
		endforeach;
		if($moreLink):
			echo '<li><a href="'.Page::findById($files[0]['pageId'])->url().'"><img src="' . URL_PUBLIC . 'public/themes/kreacjawww/images/gallery_more.png" alt="więcej" title="więcej"/></a></li>';
		endif;
		echo '</ul>';
	}
	/* 
	* method: getAlbumsArray
	* parametr: items:Array(itemId, limit, offset)
	* itemId (string or array)
	*/
	public static function getAlbumsArray($i)
	{
		if(is_array($i)):
			if(!isset($i['pageId'])) exit('no page id');
			$pageId = (is_array($i['pageId'])) ? implode(',',$i['pageId']) : $i['pageId'];
			if( (isset($i['limit'])) && (isset($i['offset'])) ):
				$offset = $i['offset'];
				$limit = $i['limit'];
				$iLimit=" LIMIT $offset,$limit "; 
			else: 
				$iLimit=""; 
			endif;
		else:
			$pageId = $i;
			$iLimit="";
		endif;
		
		$sql = "SELECT p.title, p.parent_id,  p3.* FROM ".TABLE_PREFIX."page p RIGHT JOIN 
		(
			SELECT *, count(p2.pageId) as count FROM 
			(
				SELECT p1.pageId,p1.filename, p1.fileext 
				FROM ".TABLE_PREFIX."djg_gallery_pics p1
				ORDER BY p1.sort_order ASC, p1.id DESC 
			) AS p2
			GROUP BY p2.pageId 
		) AS p3
		ON p.id = p3.pageId
		WHERE p.parent_id = $pageId ";
		$sql .= " $iLimit ";
		return Djggallery::executeSql($sql);
	} //end getPicsArray	
	public static function dispalyThemeAlbum($files=array(),$className=null,$idName=null)
	{
		$path = "public/djg_gallery/";
		$id = ($idName==null)?'':'id="'.$idName.'"';
		$class = ($className==null)?'':'class="'.$className.'"';
		echo '<ul '.$id.' '.$class.'>';
		foreach($files as $key=>$val):	
				echo "<li>";
				echo '<a href="' . Page::findById($val['pageId'])->URL() . '" title="' . $val['title'] . '">';
				echo '<img src="' . URL_PUBLIC . $path . 'thumbs' . '/145_' .$val['filename'] . '.' . $val['fileext'] . '" alt="'.$val['title'].'" width="145" height="145" /></a>';
				echo "</li>";
		endforeach;
		echo '</ul>';
	}
	
	/** helpers */
	
	public static function countPics($pageId){
		$sql = "SELECT id FROM ".TABLE_PREFIX."djg_gallery_pics WHERE pageId = $pageId";
		return count(Djggallery::executeSql($sql));
	}
	
	function newestPic($pageId){
		$sql = "SELECT * FROM ".TABLE_PREFIX."djg_gallery_pics WHERE pageId = $pageId LIMIT 1";
		return Djggallery::executeSql($sql);
	}
	

	
	
	
	
	
	
	
	
} // end class