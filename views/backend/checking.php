<?php echo "<h1>".__('Checking') . "</h1>"; ?>
<div id="djg_gallery_checking">
<?php
echo '<p>'.php_uname().'</p>';

$dir1 = CMS_ROOT.DS.'public'.DS.'djg_gallery';
$dir2 = $dir1.DS.'thumbs';
$dir3 = $dir1.DS.'watermarks';
$dir4 = $dir1.DS.'cache';

$dir_exists = '<p class="bullet_green">'.__('Directory exists').'</p>';
$dir_doesnt_exists = '<p class="bullet_red">'.__('Directory doesn\'t exists').'</p>';

$dir_is_writable = '<p class="bullet_green">'.__('Directory is writable').'</p>';
$dir_windows ='<p class="bullet_orange">'.__('Windows OS, can\'t check permitions').'</p>';
$dir_is_not_writable = '<p class="bullet_red">'.__('Directory is not writable').'</p>';

echo '<p class="folder_bug">'.$dir1.'</p>';
if ((file_exists($dir1))&&(!is_file($dir1)) ):
	echo $dir_exists;
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'):
		echo $dir_windows;
	elseif( (file_exists($dir1))&&(!is_writable($dir1)) ):
		echo $dir_is_writable;
	else:
		echo $dir_is_not_writable;
	endif;
else:
	echo $dir_doesnt_exists;
endif;

echo '<p class="folder_bug">'.$dir2.'</p>';
if ((file_exists($dir2))&&(!is_file($dir2)) ):
	echo $dir_exists;
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'):
		echo $dir_windows;
	elseif( (file_exists($dir2))&&(!is_writable($dir2)) ):
		echo $dir_is_writable;
	else:
		echo $dir_is_not_writable;
	endif;
else:
	echo $dir_doesnt_exists;
endif;

echo '<p class="folder_bug">'.$dir3.'</p>';
if ((file_exists($dir3))&&(!is_file($dir3)) ):
	echo $dir_exists;
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'):
		echo $dir_windows;
	elseif( (file_exists($dir3))&&(!is_writable($dir3)) ):
		echo $dir_is_writable;
	else:
		echo $dir_is_not_writable;
	endif;
else:
	echo $dir_doesnt_exists;
endif;

echo '<p class="folder_bug">'.$dir4.'</p>';
if ((file_exists($dir4))&&(!is_file($dir4)) ):
	echo $dir_exists;
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'):
		echo $dir_windows;
	elseif( (file_exists($dir4))&&(!is_writable($dir4)) ):
		echo $dir_is_writable;
	else:
		echo $dir_is_not_writable;
	endif;
else:
	echo $dir_doesnt_exists;
endif;
/** 
echo '<p>' . ini_get('post_max_size');
echo '<p>' . ini_get('memory_limit');
echo '<p>' . ini_get('upload_max_filesize');
*/
?>
</div>